const { setHeadlessWhen } = require('@codeceptjs/configure');

// turn on headless mode when running with HEADLESS=true environment variable
// export HEADLESS=true && npx codeceptjs run
setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './specs/matreshkaui*.spec.js', 
  //tests: './*_test.js', //путь до наших тестов
  output: './output',
  helpers: {
    Playwright: {
      url: 'http://localhost',
      show: true, //отображаем браузер или нет, в образе запускаем в headless
      browser: 'chromium' // тесты запускается в браузере хромиум,  firefox/webkit
    },


    // REST: { //добавили для тестировани апи
    //   endpoint: 'https://api.matreshka.technaxis.com', 
    // }
  },
  include: {
    I: './steps_file.js',
    mainPage: './pages/main.js', //Page Object-mainPage,  вставили после запуска команды npx codecept gpo
    currentTasksPage: './pages/currentTasks.js',
    myTicketsPage: './pages/MyTickets.js',
    myStructurPage: './pages/MyStructur.js',
  },
  bootstrap: null,
  mocha: {},
  name: 'codecept',
  plugins: {
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true //при каждом падении снова запускает 
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true //делает скриншот при каждом падении теста
    },
    allure: {
      enabled: true
    },
  }
}