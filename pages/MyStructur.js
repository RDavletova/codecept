const { I } = inject();

module.exports = {

  fields: {
      menuItemMyStructure:'a[href="/cabinet/structure"]',
      searchField: 'input[placeholder="Поиск по имени, номеру телефона и email"]',
      nameCell: { css: '.name-cell'},
  },


  searchRefferalByName(refferalName) { 

      I.click(this.fields.menuItemMyStructure); 

      I.fillField(this.fields.searchField, refferalName);

      I.pressKey('Enter');

      I.waitForElement(this.fields.nameCell, 5); // ждем пока элемент не подгрузится
    
  },

  

}
