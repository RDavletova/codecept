const { I } = inject();

module.exports = {

  fields: {
      menuItemMyTicket: 'a[href="/cabinet/tickets"]',
      searchInput: 'input[placeholder="Поиск по номеру билета"]',
      ticketNum: { css: '.ticket-num'},  
      toggler: { css: '.toggler'},
      purchaseButton: 'a[href="/cabinet/purchase"]', // кнопка Купить еще
      countTicktesField: 'input[inputmode="numeric"]', // поле ввода для числа билетов
      radioButton: locate('.radio-card').at(3), //3-ий по счету элемент
      partlySumField: locate('input').last(),
      buyButton: locate('button').withText('Купить'), // синяя кнопка

  },

  
  searchTickets(ticketNumber) { 

      I.click(this.fields.menuItemMyTicket);

      I.fillField(this.fields.searchInput, ticketNumber);

      I.pressKey('Enter');

      I.waitForElement(this.fields.ticketNum, 5); 
  },


  buyTicketWithWallet(ticketCount){ // покупка билета кошльком матрешки

    I.click(this.fields.purchaseButton);
    
    I.fillField(this.fields.countTicktesField, ticketCount);

    I.click(this.fields.buyButton); // кнопка Купить
  },


  buyTicketWithCard(ticketCount, partlySum){ // покупка билета картой

    I.click(this.fields.purchaseButton);
    I.fillField(this.fields.countTicktesField, ticketCount);
    I.click(this.fields.radioButton); // выбираем  способ оплаты картой
    
    I.fillField(this.fields.partlySumField, partlySum); // в поле вводим сумму
    
    I.click(this.fields.buyButton); // кнопка Купить
  },


  randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
  },

  
}