const { I } = inject();

module.exports = {

  fields: {
      passwordField: '[formcontrolname="password"]', //поле Пароль
      loginField: '#login-input', //поле Логин
      loginButton: locate('button').withText(' Войти '),
  },

 

  
  login(username, password) { 

      I.fillField(this.fields.loginField, username); //поле 

      I.fillField(this.fields.passwordField, password);

      I.click(this.fields.loginButton); //кликаем по кнопке this.loginButton
    
  },

  
}