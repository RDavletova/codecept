const assert = require('assert'); //для ассертов, либо chai  подключать const { expect } = require('chai');

Feature('login'); //название фичи котрую тестим

Scenario('test login', async({ I, mainPage, currentTasksPage }) => { //передаем pageObject mainPage , он  в main.js

  await I.amOnPage('https://try.vikunja.io/login'); // opens https://try.vikunja.io/login
  mainPage.login('demo', 'demo'); //вызов меода логина
  
  currentTasksPage.gotoUpcoming();
  //I.see('Labels'); // ищет по этому тексту

  I.see('Tasks from'); // ищет по этому тексту
  //I.see('Sign in to your Vikunja account on try.vikunja.io');

  //assert.equal(text1, text2);
});