const LoginService = require('./login.service'); //импорт фукнции LoginService из файла login.service.js
const Clientprofileservice = require('./clientprofile.service');

const apiProvider = () => {
  return {

    loginService: () => new LoginService(), 
    clientprofileservice: () => new Clientprofileservice(),

  };
};

module.exports = apiProvider; 

