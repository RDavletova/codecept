const { I } = inject();

const LoginService = function () { 

    this.signup = async function (params) { //  в тесте этот метод вызывам так LoginService().signup(params)
        const r = await I.sendPostRequest('/v1/client/login', params); //метод POST
        return r;
    };
};

module.exports = LoginService; //экспорт функции LoginService