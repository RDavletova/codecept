const assert = require('assert'); 
const url = 'https://matreshka.technaxis.com/auth/login';


Feature('Авторизация'); //название фичи котрую тестим
Scenario('test на авторизацию', async({ I, mainPage }) => { //передаем pageObject mainPage , он  в main.js

  await I.amOnPage(url); //open url

  mainPage.login('gfaaf@mail.ru', 'Password56'); //вызов метода логина
  
  I.see('Мой профиль'); // ищет по этому тексту
  
});



Feature('Поиск по номеру билета'); 
Scenario('test на поиск по номеру билета', async({ I, mainPage, myTicketsPage}) => { 

  await I.amOnPage(url); 

  mainPage.login('gfaaf@mail.ru', 'Password56'); 

  let ticketNumber = '81278925'; //генерить не могу
  myTicketsPage.searchTickets(ticketNumber); 

  I.see('№ ' + ticketNumber); 

});




Feature('Покупка билета кошельком'); 
Scenario('test Покупка билета кошельком', async({ I, mainPage, myTicketsPage}) => { 
  await I.amOnPage(url); 

  mainPage.login('gfaaf@mail.ru', 'Password56'); 
  let ticketCount = myTicketsPage.randomInteger(1,3);
  console.log('ticketCount ', ticketCount);

  myTicketsPage.buyTicketWithWallet(ticketCount);
  I.see('Успешно'); 
  I.see('2700');

});


Feature('Покупка билета картой'); 
Scenario('test Покупка билета картой', async({ I, mainPage, myTicketsPage}) => { 

  await I.amOnPage(url); 

  mainPage.login('gfaaf@mail.ru', 'Password56'); 

  let ticketCount = myTicketsPage.randomInteger(1,5);
  console.log('ticketCount ', ticketCount);

  let partlySum = myTicketsPage.randomInteger(100, 500);
  console.log('partlySum ', partlySum);

  myTicketsPage.buyTicketWithCard(ticketCount, partlySum);
  
  I.see('Способ оплаты'); 
  

});


Feature('Моя структура'); 
Scenario.only('test Моя структура. Поиск по имени реферала', async({ I, mainPage, myStructurPage, myTicketsPage }) => { 

  await I.amOnPage(url); 

  mainPage.login('gfaaf@mail.ru', 'Password56'); 
  const listOfNamesRefferals = [ 'Сергей', 'Евгений', 'Николай', 'Инга', 'Дамира', 'Ильнар'];

  let refferalName = istOfNamesRefferals[myTicketsPage.randomInteger(0, listOfNamesRefferals.length-1)];
  
  myStructurPage.searchRefferalByName(refferalName);

  I.see(refferalName); 

});